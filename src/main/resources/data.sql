-- create database structure here
DROP TABLE IF EXISTS car;
DROP TABLE IF EXISTS "user";

CREATE TABLE "user"
(
    id BIGSERIAL PRIMARY KEY NOT NULL,
    username VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    age INTEGER NOT NULL
);

CREATE TABLE car
(
    id BIGSERIAL PRIMARY KEY NOT NULL,
    model VARCHAR(255) NOT NULL,
    year INTEGER NOT NULL,
    image VARCHAR(255),
    user_id BIGINT REFERENCES "user"(id)
);


CREATE TABLE role
(
    id BIGSERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE user_role
(
    user_id BIGINT REFERENCES "user"(id),
    role_id BIGINT REFERENCES role(id)
);

INSERT INTO "user" (username, password, first_name, last_name, age)
VALUES ('admin', '{bcrypt}$2a$10$WJvAKW5R1VM2SSaAWf0WYO/FBcovz6X3BpulRoS2FWdUbcCZPo8V2', 'John', 'Admin', 48),
       ('user', '{bcrypt}$2a$10$WJvAKW5R1VM2SSaAWf0WYO/FBcovz6X3BpulRoS2FWdUbcCZPo8V2', 'Michael', 'User', 24);

INSERT INTO car (model, year, image, user_id)
VALUES ('Toyota Prius', 2009, null, 1),
       ('Audi 100', 2010, null, 1),
       ('BMW X3', 2011, null, 1),
       ('Audi 100', 2010, null, 2);

INSERT INTO role (name)
VALUES ('USER'),
       ('ADMIN');

INSERT INTO user_role (user_id, role_id)
VALUES (1, 1),
       (1, 2),
       (2, 1);

