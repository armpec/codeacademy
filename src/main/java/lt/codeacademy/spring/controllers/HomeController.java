package lt.codeacademy.spring.controllers;

import lombok.extern.slf4j.Slf4j;
import lt.codeacademy.spring.dtos.UserRegistrationDTO;
import lt.codeacademy.spring.entities.User;
import lt.codeacademy.spring.services.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Slf4j
@Controller
public class HomeController {

    private final UserService userService;

    public HomeController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String home(HttpServletRequest request, HttpSession session) {
        return "redirect:/cars";
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("userRegistrationDTO", new UserRegistrationDTO());
        return "register";
    }

    @GetMapping("/login")
    public String login(Model model, @AuthenticationPrincipal User user) {
        if (user == null) {
            model.addAttribute("user", new User());
            return "login";
        }

        return "redirect:/";
    }

    @PostMapping("/register")
    public String register(@Valid UserRegistrationDTO userRegistrationDTO, BindingResult bindingResult, Model model, HttpServletRequest request) throws ServletException {
        log.debug("Register request has been received. UserRegistrationDTO = {}, errors = {}, model = {}", userRegistrationDTO, bindingResult, model);

        if (bindingResult.hasErrors()) {
            log.warn("Invalid user {}", userRegistrationDTO);
            model.addAttribute("userRegistrationDTO", userRegistrationDTO);
            return "register";
        }

        if (!userRegistrationDTO.getPassword().equals(userRegistrationDTO.getPasswordConfirm())) {
            bindingResult.addError(new ObjectError("userRegistrationDTO", "Passwords do not match"));
            model.addAttribute("userRegistrationDTO", userRegistrationDTO);
            return "register";
        }

        if (userService.createUser(userRegistrationDTO) == null) {
            bindingResult.addError(new ObjectError("user", String.format("User with username '%s' already exists", userRegistrationDTO.getUsername())));
            model.addAttribute("userRegistrationDTO", userRegistrationDTO);
            return "register";
        }

        log.info("User {} has registered successfully", userRegistrationDTO);
        request.login(userRegistrationDTO.getUsername(), userRegistrationDTO.getPassword());
        return "redirect:/";
    }
}
