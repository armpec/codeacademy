package lt.codeacademy.spring.controllers;

import lt.codeacademy.spring.entities.Car;
import lt.codeacademy.spring.services.CarService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/test")
public class TestController {

    private final CarService carService;

    public TestController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping("/redirect")
    public String testRedirect(@RequestParam(required = false) String test) {
        return "redirect:/cars";
    }

    @GetMapping("/forward")
    public String testForward(@RequestParam(required = false) String test) {
        return "forward:/cars";
    }

    @GetMapping("/page")
    @ResponseBody
    public Page<Car> test(@PageableDefault(size = 5) Pageable pageable) {
        return carService.getCarsPaginated(pageable, null);
    }
}
