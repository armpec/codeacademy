package lt.codeacademy.spring.controllers;

import lombok.extern.slf4j.Slf4j;
import lt.codeacademy.spring.exceptions.CarNotFoundException;
import lt.codeacademy.spring.exceptions.UserNotFoundException;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@ControllerAdvice
public class CustomControllerAdvice {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({CarNotFoundException.class, UserNotFoundException.class})
    public String notFound(Exception e, WebRequest request, Model model) {
        if (e instanceof CarNotFoundException) {
            log.error("Car with id = {} was not found", ((CarNotFoundException) e).getCarId(), e);
            model.addAttribute("carId", ((CarNotFoundException) e).getCarId());
            return "carNotFound";
        } else if (e instanceof UserNotFoundException) {
            model.addAttribute("username", ((UserNotFoundException) e).getUsername());
            return "userNotFound";
        }

        return "error";
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(false);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy --- MM --- dd");
        CustomDateEditor customDateEditor = new CustomDateEditor(simpleDateFormat, true);
        dataBinder.registerCustomEditor(Date.class, customDateEditor);
    }

    @ModelAttribute("now")
    public Date now() {
        return new Date();
    }

//    @ModelAttribute("user")
//    public User user() {
//        return new User();
//    }
}
