package lt.codeacademy.spring.controllers;

import lt.codeacademy.spring.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getUsers(@RequestParam(required = false) String firstName, @RequestParam(required = false) Integer age, Model model) {
//        model.addAttribute("users", userService.getUsers(firstName, age));
        return "users";
    }

    @GetMapping("/{id}")
    public String getUser(@PathVariable Long id, Model model) {
//        model.addAttribute("user", userService.getUser(id));
        return "user";
    }
}
