package lt.codeacademy.spring.controllers;

import lt.codeacademy.spring.dtos.CarDTO;
import lt.codeacademy.spring.entities.Car;
import lt.codeacademy.spring.entities.User;
import lt.codeacademy.spring.services.CarService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;

@Controller
@SessionAttributes({"theme"})
@RequestMapping("/cars")
public class CarController {

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping
    public String getCars(@PageableDefault(size = 10) Pageable pageable,
                          @RequestParam(required = false) Integer year,
                          @RequestParam(required = false, value = "search") String searchStr,
                          @AuthenticationPrincipal User user,
                          Model model,
                          HttpSession session,
                          HttpServletRequest request,
                          HttpServletResponse response) {
        model.addAttribute("carsPage", carService.getCarsPaginated(pageable, year));
        return "cars/index";
    }

    @GetMapping("/user")
    public String getCarsByUser(@PageableDefault(size = 10) Pageable pageable,
                                @AuthenticationPrincipal User user,
                                Model model) {
        model.addAttribute("carsPage", carService.getCarsByUserPaginated(pageable, user));
        return "cars/index";
    }

    @GetMapping("/create")
    public String createCar(Model model) {
        model.addAttribute("carDTO", new CarDTO());
        return "cars/create";
    }

    @GetMapping("/{id}/view")
    public String getCar(@PathVariable("id") Car car, Model model) {
        model.addAttribute("car", car);
        return "cars/view";
    }

    @PreAuthorize("hasRole('ADMIN') || principal.id == #car.user.id")
    @GetMapping("/{id}/edit")
    public String editCar(@PathVariable("id") Car car, Model model) {
        model.addAttribute("car", car);
        return "cars/edit";
    }

    @PreAuthorize("hasRole('ADMIN') || principal.id == #car.user.id")
    @GetMapping("/{id}/delete")
    public String deleteCar(@PathVariable("id") Car car) {
        carService.deleteCar(car);
        return "redirect:/cars";
    }

    @PostMapping("/create")
    public String createCar(@Valid CarDTO carDTO, BindingResult bindingResult, Model model, RedirectAttributes attributes) throws IOException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("carDTO", carDTO);
            return "cars/create";
        }

        Car car = carService.saveCar(carDTO);
        attributes.addFlashAttribute("successMsg", "Your car has been successfully created!");
        return "redirect:/cars/" + car.getId() + "/view";
    }

    @PreAuthorize("hasRole('ADMIN') || principal.id == #car.user.id")
    @PostMapping("/{id}/edit")
    public String editCar(@PathVariable("id") Car car, @Valid CarDTO carDTO, BindingResult bindingResult, Model model) throws IOException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("carDTO", carDTO);
            return "cars/edit";
        }

        return "redirect:/cars/" + carService.saveCar(carDTO).getId() + "/view";
    }
}
