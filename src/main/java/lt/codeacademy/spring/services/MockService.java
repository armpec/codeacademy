package lt.codeacademy.spring.services;

import lt.codeacademy.spring.entities.Car;
import lt.codeacademy.spring.entities.Company;
import lt.codeacademy.spring.entities.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Deprecated
public class MockService {

    private final List<Car> cars = new ArrayList<>();

    private final List<Company> companies = new ArrayList<>();

    private final List<User> users = new ArrayList<>();

    public MockService() {

    }

    public List<Car> getCars() {
        return cars;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public List<User> getUsers() {
        return users;
    }

    public Car createCar(Car car) {
        car.setId(System.currentTimeMillis() / 1000L);
        cars.add(car);
        return car;
    }
}
