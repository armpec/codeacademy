package lt.codeacademy.spring.services;

import lt.codeacademy.spring.dtos.CarDTO;
import lt.codeacademy.spring.entities.Car;
import lt.codeacademy.spring.entities.User;
import lt.codeacademy.spring.exceptions.CarNotFoundException;
import lt.codeacademy.spring.repositories.CarJDBCRepository;
import lt.codeacademy.spring.repositories.CarRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

@Service
public class CarService {

    private final StorageService storageService;

    private final CarRepository carRepository;

    private final CarJDBCRepository carJDBCRepository;

    public CarService(StorageService storageService, CarRepository carRepository, CarJDBCRepository carJDBCRepository) {
        this.storageService = storageService;
        this.carRepository = carRepository;
        this.carJDBCRepository = carJDBCRepository;
    }

    //TODO refactor
    public Page<Car> searchCarsPaginated(Pageable pageable, String searchStr) {
        return searchStr != null ? carRepository.getCarsByModelContaining(pageable, searchStr) : carRepository.findAll(pageable);
    }

    public Page<Car> getCarsPaginated(Pageable pageable, Integer year) {
        return year != null ? carRepository.getCarsByYear(pageable, year) : carRepository.findAll(pageable);
    }

    public Page<Car> getCarsByUserPaginated(Pageable pageable, User user) {
        return carRepository.getCarsByUser(pageable, user);
    }

    public Car getCar(Long id) {
        return carRepository.getCarById(id).orElseThrow(() -> new CarNotFoundException(id));
    }

    public List<Car> getCarsByExample() {
        Car car = new Car();
        car.setModel("TOYOTA PRIUS");
        car.setYear(2009);

        ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase();

        return carRepository.findAll(Example.of(car, matcher));
    }

    @Transactional
    public Car saveCar(CarDTO carDTO) throws IOException {
        Car car = new Car(carDTO);

        if (!carDTO.getImageFile().isEmpty()) {
            carRepository.save(car);
            car.setImage(storageService.save(carDTO.getImageFile()));
        }

        return carRepository.save(car);
    }

    public Car saveCar(Car car) {
        return carRepository.save(car);
    }

    public void deleteCar(Car car) {
        carRepository.delete(car);
    }

    public void createCars(List<Car> cars) {
        carRepository.saveAll(cars);
    }
}
