package lt.codeacademy.spring.services;

import lt.codeacademy.spring.entities.Company;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    private final MockService mockService;

    public CompanyService(MockService mockService) {
        this.mockService = mockService;
    }

    public List<Company> getCompanies() {
        return mockService.getCompanies();
    }
}
