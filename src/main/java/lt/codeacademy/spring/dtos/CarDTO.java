package lt.codeacademy.spring.dtos;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;

@Data
public class CarDTO {

    private Long id;

    @NotBlank
    private String model;

    @Range(min = 1900, max = 2100)
    private Integer year;

    private String image;

    private MultipartFile imageFile;
}
