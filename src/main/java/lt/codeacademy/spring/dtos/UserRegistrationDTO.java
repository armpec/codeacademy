package lt.codeacademy.spring.dtos;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.*;

@Data
public class UserRegistrationDTO {

    @NotBlank
    private String username;

    @Size(min = 6, max = 30, message = "{validation.size.user.password}")
    private String password;

    @Size(min = 6, max = 30, message = "{validation.size.user.password}")
    private String passwordConfirm;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotNull
    @Range(min = 1, max = 140)
    private Integer age;
}
