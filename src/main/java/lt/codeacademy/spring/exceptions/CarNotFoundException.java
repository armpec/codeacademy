package lt.codeacademy.spring.exceptions;

public class CarNotFoundException extends RuntimeException {

    private final Long carId;

    public CarNotFoundException(Long carId) {
        this.carId = carId;
    }

    public Long getCarId() {
        return carId;
    }
}
