package lt.codeacademy.spring.entities;

import lombok.Data;
import lt.codeacademy.spring.dtos.CarDTO;

import javax.persistence.*;

@Data
@Entity
@Table(name = "car")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "model")
    private String model;

    @Column(name = "year")
    private Integer year;

    @Column(name = "image")
    private String image;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Car() {
    }

    public Car(CarDTO carDTO) {
        this.id = carDTO.getId();
        this.model = carDTO.getModel();
        this.year = carDTO.getYear();
        this.image = carDTO.getImage();
    }

    public Car(String model, Integer year) {
        this.model = model;
        this.year = year;
    }
}
