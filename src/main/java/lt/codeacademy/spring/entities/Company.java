package lt.codeacademy.spring.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
public class Company {

    private Long id;

    private String name;

    private List<Car> cars;

    private List<User> users;

    private List<Gps> gpsList;

    public Company(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
