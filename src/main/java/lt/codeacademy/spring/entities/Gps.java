package lt.codeacademy.spring.entities;

import lombok.Data;

@Data
public class Gps  {

    private Long id;

    private String model;

    private Double latitude;

    private Double longitude;

    private Company company;

    private Car car;
}
