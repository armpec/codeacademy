package lt.codeacademy.spring.repositories;

import lt.codeacademy.spring.entities.Car;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class CarJDBCRepository {

    class CarRowMapper implements RowMapper<Car> {

        @Override
        public Car mapRow(ResultSet resultSet, int i) throws SQLException {
            Car car = new Car();
            car.setId(resultSet.getLong("id"));
            car.setModel(resultSet.getString("model"));
            car.setYear(resultSet.getInt("year"));
            return car;
        }
    }

    private final JdbcTemplate jdbcTemplate;

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public CarJDBCRepository(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public Car getCarById(Long id) {
        return jdbcTemplate.queryForObject("select * from car where id = ?", new CarRowMapper(), id);
    }

    public Car getCarByIdWithNamedParameters(Long id) {
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("id", id);
        return namedParameterJdbcTemplate.queryForObject("select * from car where id = :id", namedParameters, new CarRowMapper());
    }

    public int[] createCars(List<Car> cars) {
        return jdbcTemplate.batchUpdate("INSERT INTO car (model, year) VALUES (?, ?)", new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, cars.get(i).getModel());
                ps.setInt(2, cars.get(i).getYear());
            }

            @Override
            public int getBatchSize() {
                return cars.size();
            }
        });
    }
}
