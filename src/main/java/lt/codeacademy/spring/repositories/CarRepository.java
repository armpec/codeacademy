package lt.codeacademy.spring.repositories;

import lt.codeacademy.spring.entities.Car;
import lt.codeacademy.spring.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    // select * from car where model = :model
    List<Car> getCarsByModel(String model);

    // select * from car where model like :str
    Page<Car> getCarsByModelContaining(Pageable pageable, String str);

    // select * from car where year = :year
    Page<Car> getCarsByYear(Pageable pageable, Integer year);

    Page<Car> getCarsByUser(Pageable pageable, User user);

    Page<Car> getCarsByUserAndYear(Pageable pageable, User user, Integer year);

    Optional<Car> getCarById(Long id);

    Optional<Car> getCarByYear(Integer year);

//    @Query("SELECT c FROM Car c WHERE c.model = ?1 ORDER BY RANDOM()")
//    @Query(value = "SELECT * FROM car WHERE model = ?1 ORDER BY RANDOM()", nativeQuery = true)
    @Query(value = "SELECT * FROM car WHERE model = :model ORDER BY RANDOM()", nativeQuery = true)
    List<Car> getCarsByModelOrderByRandom(@Param("model") String model);
}
