package lt.codeacademy.spring.filters;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.servlet.*;
import java.io.IOException;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@Component
public class TimeElapsedFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("Request has been received: url = {}", servletRequest.getRemoteAddr());

        StopWatch stopWatch = new StopWatch();

        stopWatch.start();
        filterChain.doFilter(servletRequest, servletResponse);
        stopWatch.stop();

        log.info("Request has been successfully processed in {} ms!", stopWatch.getLastTaskTimeMillis());
    }
}
